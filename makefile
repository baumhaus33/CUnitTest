
 # project: CUintTest
 # created: Thu Jul 26 13:33:44 2018
 # creator: christian


PROG = a.out #this is the program name
CC = gcc #this is the compiler
OBJ = bin/sum.o bin/main.o
SUM_TEST = tests/check_sum

$(PROG): $(OBJ)
	$(CC) -o $(PROG) $(OBJ)

bin/main.o: src/main.c
	$(CC) -c src/main.c -o bin/main.o

bin/sum.o: src/sum.c
	$(CC) -c src/sum.c -o bin/sum.o

clean:
	rm bin/*.o
	rm $(SUM_TEST)

sum_test:
	# build the hole thing to get the neede objects
	make
	# build check_sum
	$(CC) -o bin/check_sum.o -c tests/check_sum.c
	# bind it to executalbe
	gcc bin/sum.o bin/check_sum.o -lcheck -lm -lpthread -lrt -lsubunit -o $(SUM_TEST)
	# execute the test
	./$(SUM_TEST)
